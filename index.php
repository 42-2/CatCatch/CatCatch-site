<?php

/**
 * Make sure you started your'e sessions!
 * You need to include su.inc.php to make SimpleUsers Work
 * After that, create an instance of SimpleUsers and your'e all set!
 */

session_start();
require_once(dirname(__FILE__)."/account/simpleusers/su.inc.php");

$SimpleUsers = new SimpleUsers();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
        <title>CatCatch, a game by 42/2!</title>
        <?php include('partials/stylesheets.html'); ?>
    </head>
    <body>
        <?php
            include('partials/javascripts.html');
            include('partials/menu.php');
        ?>
        <main role="main" class="container">
            <h1 class="mt-6 text-center">CatCatch, a game by 42/2!</h1>
        </main>
        <br />
        <div class="container">
            <div class="text-center">
                <h2>Watch our trailers right here!</h2>
                <br/>
                <div class="row">
                    <div class="col-sm-6"><iframe width="560" height="315" src="https://www.youtube.com/embed/G59uuY08oMk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
                    <div class="col-sm-6"><iframe width="560" height="315" src="https://www.youtube.com/embed/AxK7ZTJnsws" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
                </div>
            </div>

            <br/>
            <br/>
            <div class="row">
                <div class="col-md-4">
                    <div class="card w-100 mb-4 box-shadow h-100">
                        <img class="card-img-top" src="/assets/img/cats/Hervot_Cat.png" alt="CatCatch in short">
                        <div class="card-body">
                            <h5 class="card-title">CatCatch in short</h5>
                            <p class="card-text text-justify">
                                CatCatch is a multiplayer video game in the third person view. It will consist in a chase
                                game, implying two types of players: the chasers and the runners. In order to win, the
                                chasers will have to catch the highest number of runners. On their side, the runners will
                                have to survive for the longest time, in the best case until the end of the time limit. The
                                characters used by the players will be cats, modelized in a modelling-clay-like style. A
                                variety of different cats will be available for the players to choose, each with different
                                particular capacities that will allow them to better chase, or better run away. The idea is
                                that each chaser cat is inspired from a teacher of EPITA and each runner cat is inspired
                                from a student from the 2022 EPITA Strasbourg class.
                            </p>
                            <a href="/the-game.php" class="btn btn-primary">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card w-100 mb-4 box-shadow h-100">
                        <img class="card-img-top" src="/assets/img/team-logo.png" alt="42/2 Team">
                        <div class="card-body">
                            <h5 class="card-title">The 42/2 Team</h5>
                            <p class="card-text text-justify">
                                Four students of EPITA constitute the 42/2 team : Marc Schmitt, Paul Decrosse, Jean
                                Troffer-Charlier and Benjamin Decreusefond. The name of the team comes from two references :
                                "42", because it is the answer to the Ultimate Question about Life, the Universe, and
                                Everything, and "/2" because 42/2 = 21, and 21 happens to be the name of the team's
                                favourite shot bar.
                            </p>
                            <a href="/about-us.php" class="btn btn-primary">About us</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card w-100 mb-4 box-shadow h-100">
                        <img class="card-img-top" src="/assets/img/cats/Marc_Cat.png" alt="CatCatch in short">
                        <div class="card-body">
                            <h5 class="card-title text-justify">Gameplay</h5>
                            <p class="card-text">
                                Speed boni, or mali to launch on opponents will randomly appear on the map. The chasers will
                                have a limited time to catch and eliminate all the runners, and the runners will have to run
                                away from the chasers until the time limit is reached. In order to catch a runner, a chaser
                                will have to be at a close range from him and push a button to tackle him. The cats’ special
                                capacities will help them, either to run or to catch: teleporting, camouflage, go faster, or
                                make opponent go slower. . . The capacities will be usable on the cats of the same type, to
                                handicap them and have them making a less good score. Boni and mali will allow players to
                                turn the situation over to their advantage.
                            </p>
                            <a href="/gallery.php" class="btn btn-primary">Gallery</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <?php include('partials/footer.html'); ?>
    </body>
</html>
<!--{# TODO: change photos and add a banner #}-->
<!--{# TODO: make html look good plz #}-->