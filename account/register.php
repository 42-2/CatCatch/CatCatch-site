<?php

	/**
	* Make sure you started your'e sessions!
	* You need to include su.inc.php to make SimpleUsers Work
	* After that, create an instance of SimpleUsers and your'e all set!
	*/

	session_start();
	require_once(dirname(__FILE__)."/simpleusers/su.inc.php");

	$SimpleUsers = new SimpleUsers();

	// Validation of input
	if( isset($_POST["username"]) )
	{
		if( empty($_POST["username"]) || empty($_POST["password"]) )
			$error = "You have to choose a username and a password";
    else
    {
    	// Both fields have input - now try to create the user.
    	// If $res is (bool)false, the username is already taken.
    	// Otherwise, the user has been added, and we can redirect to some other page.
			$res = $SimpleUsers->createUser($_POST["username"], $_POST["password"]);

			if(!$res)
				$error = "Username already taken.";
			else
			{
					header("Location: login.php");
					exit;
			}
		}

	} // Validation end

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <title>Register | CatCatch, a game by 42/2!</title>
    <?php include('../partials/stylesheets.html'); ?>
</head>
<body>
<?php
include('../partials/javascripts.html');
include('../partials/menu.php');
?>
<main role="main" class="container">
    <h1 class="mt-6 text-center">Register</h1>
</main>
<br />
<div class="container">
		<?php if( isset($error) ): ?>
		<p>
			<?php echo $error; ?>
		</p>
		<?php endif; ?>

		<form method="post" action="">
			<p>
				<label for="username">Username:</label><br />
				<input type="text" name="username" id="username" />
			</p>

			<p>
				<label for="password">Password:</label><br />
				<input name="password" id="password" type="password"/>
			</p>

			<p>
				<input type="submit" name="submit" value="Register" />
			</p>

		</form>

</div>
<br />
<?php include('../partials/footer.html'); ?>
</body>
</html>