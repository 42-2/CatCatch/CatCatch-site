<?php

/**
 * Make sure you started your'e sessions!
 * You need to include su.inc.php to make SimpleUsers Work
 * After that, create an instance of SimpleUsers and your'e all set!
 */

session_start();
require_once(dirname(__FILE__)."/account/simpleusers/su.inc.php");

$SimpleUsers = new SimpleUsers();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
        <title>Download | CatCatch, a game by 42/2!</title>
        <?php include('partials/stylesheets.html'); ?>
    </head>
    <body>
        <?php
            include('partials/javascripts.html');
            include('partials/menu.php');
        ?>
        <main role="main" class="container">
            <h1 class="mt-6 text-center">Download</h1>
        </main>
        <br />
        <div class="container">
            <div class="row">

                <div class="col-sm-2  d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/windows-64.png" alt="Win64">
                        <div class="card-body">
                            <h5 class="card-title">Windows 64-bits</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-installer-win-x86_64-v3.0.zip" class="btn btn-primary" download>Installer</a><br /><br />
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-win-x86_64-v3.0.zip" class="btn btn-primary" download>Executable</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2   d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/windows-32.png" alt="Win32">
                        <div class="card-body">
                            <h5 class="card-title">Windows 32-bits</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-installer-win-x86-v3.0.zip" class="btn btn-primary" download>Installer</a><br /><br />
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-win-x86-v3.0.zip" class="btn btn-primary" download>Executable</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/linux-64.png" alt="Linux64">
                        <div class="card-body">
                            <h5 class="card-title">Linux 64-bits</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-linux-x86_64-v3.0.zip" class="btn btn-primary" download>Executable</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/linux-32.png" alt="Linux32">
                        <div class="card-body">
                            <h5 class="card-title">Linux 32-bits</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-linux-x86-v3.0.zip" class="btn btn-primary" download>Executable</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/macos.png" alt="MacOS">
                        <div class="card-body">
                            <h5 class="card-title">MacOS</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-macos-v3.0.zip" class="btn btn-primary" download>Executable</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 d-flex align-items-stretch">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="/assets/builds/icons/html5.png" alt="WebGL">
                        <div class="card-body">
                            <h5 class="card-title">WebGL</h5>
                            <h6 class="card-subtitle mb-2 text-muted">v2.0</h6>
                            <p class="card-text">You can try our game online!!!</p>
                            <a href="/play-online/" class="btn btn-primary">Play now!</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <br />

        <div class="container">
            <ul>
                <li>
                    <i class="fa fa-file-alt"></i> User manual <a href="https://gitlab.com/42-2/CatCatch/CatCatch-executables/raw/master/final-defense/CatCatch-User-Manual-v3.0.pdf" target="_blank" download>here</a> at all times.
                </li>
                <li>
                    <i class="fa fa-code"></i> Source code is available <a href="https://gitlab.com/42-2/CatCatch/CatCatch" target="_blank">here</a> at all times.
                </li>
                <li>
                    <i class="fa fa-file-alt"></i> Dossiers, such as specifications, defense reports and plans, are available <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs" target="_blank">here</a> at all times.
                </li>
                <li>
                    <i class="fa fa-globe"></i> Source code for this website is available <a href="https://gitlab.com/42-2/CatCatch/CatCatch-site" target="_blank">here</a> at all times.
                </li>
            </ul>
        </div>
        <br/>
        <div class="container">
            <h3>Timeline of the CatCatch project</h3>
            <ul class="timeline">
                <li>
                    <div class="timeline-badge success"><i class="fa fa-thumbs-up"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Final Defense</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> June 10<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Source code and executables used for the defense are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch/tags/Final-Defense" target="_blank">here</a>.<br/>
                                Defense plan is available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v3.1" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

                <li class="timeline-inverted">
                    <div class="timeline-badge success"><i class="fa fa-file-alt"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Project Report</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> May 25<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Project report is available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v3.0" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="timeline-badge primary"><i class="fa fa-check"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Second Defense</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> May 4<sup>th</sup> (be with you) 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Source code and executables used for the defense are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch/tags/Second-Defense-Build" target="_blank">here</a>.<br/>
                                Updated specifications, defense plan and defense report are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v2.0" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

                <li class="timeline-inverted">
                    <div class="timeline-badge danger"><i class="fa fa-thumbs-down"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">First Defense</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> March 14<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Source code and executables used for the defense are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch/tags/v0.1" target="_blank">here</a>.<br/>
                                Updated specifications, defense plan and defense report are available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v1.0" target="_blank">here</a>.<br/>
                            </p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="timeline-badge info"><i class="fa fa-file-alt"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Specifications</h4>
                            <p>
                                <small class="text-muted"><i class="fa fa-clock"></i> January 19<sup>th</sup> 2018</small>
                            </p>
                        </div>
                        <div class="timeline-body">
                            <p>Available for download <a href="https://gitlab.com/42-2/CatCatch/CatCatch-docs/tags/v0.5" target="_blank">here</a>.</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <br />
        <?php include('partials/footer.html'); ?>
    </body>
</html>
