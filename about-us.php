<?php

	/**
	* Make sure you started your'e sessions!
	* You need to include su.inc.php to make SimpleUsers Work
	* After that, create an instance of SimpleUsers and your'e all set!
	*/

	session_start();
	require_once(dirname(__FILE__)."/account/simpleusers/su.inc.php");

	$SimpleUsers = new SimpleUsers();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
        <title>About us | CatCatch, a game by 42/2!</title>
        <?php include('partials/stylesheets.html'); ?>
    </head>
    <body>
        <?php
            include('partials/javascripts.html');
            include('partials/menu.php');
        ?>
        <div class="container"><img class="img-fluid" src="/assets/img/team-members.jpg" alt="42/2 members"></div>
        <br />
        <main role="main" class="container">
            <h1 class="mt-6 text-center">About us</h1>
        </main>
        <br />
        <div class="container">
            <img class="img-fluid align-items-center" src="/assets/img/team-logo.png" style="width:15%;text-align:center;margin-left:42.5%" />
            <br/><br/>
            <p class="text-justify">
                Four students of EPITA constitute the 42/2 team (from left to right): Jean Troffer-Charlier, Marc Schmitt, Paul Decrosse and
                Benjamin Decreusefond. The name of the team comes from two references : "42", because it is the answer to
                the Ultimate Question about Life, the Universe, and Everything, and "/2" because 42/2 = 21, and 21 happens
                to be the name of the team's favourite shot bar.
            </p>
        </div>
        <br />
        <?php include('partials/footer.html'); ?>
    </body>
</html>