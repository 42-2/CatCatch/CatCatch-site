<?php

/**
 * Make sure you started your'e sessions!
 * You need to include su.inc.php to make SimpleUsers Work
 * After that, create an instance of SimpleUsers and your'e all set!
 */

session_start();
require_once(dirname(__FILE__)."/account/simpleusers/su.inc.php");

$SimpleUsers = new SimpleUsers();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
        <title>Gallery | CatCatch, a game by 42/2!</title>
        <?php include('partials/stylesheets.html'); ?>
    </head>
    <body>
        <?php
            include('partials/javascripts.html');
            include('partials/menu.php');
        ?>
        <main role="main" class="container">
            <h1 class="mt-6 text-center">Gallery</h1>
        </main>
        <br />

        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick-theme.css"/>

<!--        {#    TODO: replace partner and partners by something that actually makes sense (don't forget the CSS and script at the end of this file too #}-->
        <div class="partners">
            <?php
            $directory = 'assets/img/gallery-img/';
            $scanned_directory = array_slice(scandir($directory), 2);
            shuffle($scanned_directory);
            foreach ($scanned_directory as $slide)
            {
                echo '<div class="partner">';
                echo '<img class="partner" src="' . $directory . $slide . '" alt="CatchCatch screenshot">';
                echo '</div>';
            }
            ?>
        </div>

        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.partners').slick({
                    infinite: true,
                    variableWidth: true,
                    autoplay: true,
                    pauseOnFocus: false,
                    pauseOnHover: false,
                });
            });
        </script>
        <br />
        <?php include('partials/footer.html'); ?>
    </body>
</html>