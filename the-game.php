<?php

/**
 * Make sure you started your'e sessions!
 * You need to include su.inc.php to make SimpleUsers Work
 * After that, create an instance of SimpleUsers and your'e all set!
 */

session_start();
require_once(dirname(__FILE__)."/account/simpleusers/su.inc.php");

$SimpleUsers = new SimpleUsers();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
        <title>The Game | CatCatch, a game by 42/2!</title>
        <?php include('partials/stylesheets.html'); ?>
    </head>
    <body>
        <?php
            include('partials/javascripts.html');
            include('partials/menu.php');
        ?>
        <div class="container"><img class="img-fluid" src="/assets/img/misc/Multijoueur.png" alt="42/2 members"></div>
        <br />
        <main role="main" class="container">
            <h1 class="mt-6 text-center">The Game</h1>
        </main>
        <br />
        <div class="container">
            <p class="text-justify">
                CatCatch is a multiplayer video game in the third person view. It will consist in a chase game, implying two
                types of players: the chasers and the runners. In order to win, the chasers will have to catch the highest
                number of runners. On their side, the runners will have to survive for the longest time, in the best case
                until the end of the time limit. The characters used by the players will be cats, modelized in a
                modelling-clay-like style. A variety of different cats will be available for the players to choose, each
                with different particular capacities that will allow them to better chase, or better run away. The idea is
                that each chaser cat is inspired from a teacher of EPITA and each runner cat is inspired from a student from
                the 2022 EPITA Strasbourg class.
            </p>
            <p class="text-justify">
                Speed boni, or mali to launch on opponents will randomly appear on the map. The chasers will have a limited
                time to catch and eliminate all the runners, and the runners will have to run away from the chasers until
                the time limit is reached. In order to catch a runner, a chaser will have to be at a close range from him
                and push a button to tackle him. The cats’ special capacities will help them, either to run or to catch:
                teleporting, camouflage, go faster, or make opponent go slower. . . The capacities will be usable on the
                cats of the same type, to handicap them and have them making a less good score. Boni and mali will allow
                players to turn the situation over to their advantage. If all the runners are caught before the end of the
                time, players will be ranked as following: first the chasers, ranked according to the number of runners they
                caught, then the runners, ranked according to the time they survived, and then the chasers having caught no
                runner. If the time limit is reached, the players will be ranked as following: first the remaining runners,
                then the runners who have been caught, ranked according to their surviving time, and then the chasers,
                ranked according on the number of runners they have caught.
            </p>
        </div>
        <br />
        <?php include('partials/footer.html'); ?>
    </body>
</html>
<!--{#TODO: add a photo#}-->
<!--{#TODO: add trailer when ready#}-->
